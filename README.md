# Wikipedia Change User Skin

Automatically changes the skin for Wikipedia using userscripts or the Redirector extension.

## Userscripts

1. Install a userscript manager (Violentmonkey, Greasemonkey, or Tampermonkey)
   if you don't have one already

2. Install the [userscript](https://codeberg.org/archeite/wikipedia-change-skin/raw/branch/main/main.user.js)

### Configuration

Change the `skin` variable either to

- `vector2022`
- `vector`
- `monobook`
- `timeless`
- `minerva`

*Previews are avaliable at [wikipedia.org/wiki/Wikipedia:Skin](https://wikipedia.org/wiki/Wikipedia:Skin)*

## Redirector

1. Install the extension:
   [Firefox](https://addons.mozilla.org/en-GB/firefox/addon/redirector/) or
   [Chrome](https://chrome.google.com/webstore/detail/redirector/ocgpenflpmgnfapjedencafcfakcekcd)

2. Click on the Redirector extension icon, click *Edit Redirects*, click
   *Create new redirect*, then fill in the following:

```yaml
Description:     Wikipedia Custom Skin
Example URL:     https://en.wikipedia.org/wiki/Cat
Include Pattern: *://*.wikipedia.org/*
Redirect to:     https://en.wikipedia.org/$3?useskin=!placeholder!
Pattern type:    Wildcard
Pattern Description: Automatically changes the skin for Wikipedia.

Exclude pattern (Advanced Options): *://*.wikipedia.org/*?useskin=*
```

3. Replace `!placeholder!` with one of these options:

- `vector2022`
- `vector`
- `monobook`
- `timeless`
- `minerva`

*Previews are avaliable at [wikipedia.org/wiki/Wikipedia:Skin](https://wikipedia.org/wiki/Wikipedia:Skin)*

## Caveats

Because this modifies the URL to change the userskin, this would affect the tab
history as well (going back to the previous page, etc.).

## License

Licensed under the [GPLv3](https://codeberg.org/archeite/wikipedia-change-skin/raw/branch/main/LICENSE).
